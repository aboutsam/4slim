<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExampleCommand extends Command
{
    // the name of the command (the part after "bin/console")
    // protected static $defaultName = 'app:create-user';

    protected function configure()
    {
        $this->setName('test:file-creation');
        $this->setDescription('Generate random .txt file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        $txt = "First Line\n";
        fwrite($myfile, $txt);
        $txt = "Second Line\n";
        fwrite($myfile, $txt);
        fclose($myfile);
        return 1;
    }
}
